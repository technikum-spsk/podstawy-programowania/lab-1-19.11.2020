cmake_minimum_required(VERSION 3.17)
project(Lab_1_19_11_2020)

set(CMAKE_CXX_STANDARD 20)

add_executable(Lab_1_19_11_2020 main.cpp)