#include <iostream>

using namespace std;

void display(bool table[], int j);

void tableOverwrite(bool table[], int j, int k);

void display2d(bool table[2][3]);

void table2dOverwrite(bool table[2][3]);

int main() {
    bool table[5] = {false, true, false, true, false};
    cout << "Zad 1" << endl;
    display(table, 5);
    cout << "Zad 2" << endl;
    tableOverwrite(table, 5, 1);
    display(table, 5);
    cout << "Zad 3" << endl;
    bool table2D[2][3] = {
            {true, false, true},
            {true, false, true}
    };
    display2d(table2D);
    cout << "Zad 4" << endl;
    table2dOverwrite(table2D);
    display2d(table2D);
    return 0;
}

void display(bool table[], int j) {
    for (int i = 0; i < j; i++) {
        cout << "[" << i << "]: " << table[i] << endl;
    }
}

void tableOverwrite(bool table[], int j, int k) {
    for (int i = 0; i < j; i++) {
        table[i] = k;
    }
}

void display2d(bool table[2][3]) {
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 3; j++) {
            cout << "[" << i << "][" << j << "]: " << table[i][j] << endl;
        }
    }
}

void table2dOverwrite(bool table[2][3]) {
    for (int i = 0; i < 3; i++) {
        table[0][i] = 'x';
    }

    for (int j = 0; j < 3; j++) {
        table[1][j] = 'y';
    }
}